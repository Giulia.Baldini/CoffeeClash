// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;

/**
 * This file is the template for the company strategies and it should not be modified
 */
public abstract class Company {
    private int id;
    private double budget;
    private MarketingCampaign campaign;
    protected MarketingConfig config;

    /**
     * Constructor for the Company class
     *
     * @param id          the ID of the company
     * @param startBudget the budget that a company can spend during the time period
     * @param campaign    the campaign object where this company will run
     */
    public Company(int id, double startBudget, MarketingCampaign campaign) {
        this.id = id;
        this.budget = startBudget;
        this.campaign = campaign;
        this.config = campaign.config;
        initialize();
    }

    /**
     * Getter for the ID of the company
     *
     * @return the ID of the company
     */
    final protected int getId() {
        return id;
    }

    /**
     * Getter for the budget of the company
     *
     * @return the current budget of the company
     */
    final protected double getBudget() {
        return budget;
    }

    /**
     * Getter for the current day of the campaign
     *
     * @return the current day of the campaign
     */
    final protected int getCurrentDay() {
        return campaign.getCurrentDay();
    }

    /**
     * Does research on the market by asking buyers which company they prefer
     *
     * @param buyersToAsk list of buyer IDs to ask
     * @return a list of answers corresponding to the same order of given IDs,
     * the values are companyId or -1 in case of insufficient budget
     */
    final protected ArrayList<Integer> research(ArrayList<Integer> buyersToAsk) {
        ArrayList<Integer> answers = new ArrayList<>();
        for (int buyerId : buyersToAsk) {
            if (charge(campaign.config.getResearchCharge())) {
                answers.add(campaign.getBuyer(buyerId).ask());
            } else {
                answers.add(-1);
            }
        }
        return answers;
    }

    /**
     * Convinces multiple buyers of a company's product
     *
     * @param buyersToConvince list of buyer IDs to convince
     * @param times            number of times to convince
     * @return an array that, for each buyer, has true if the company managed to
     * convince the buyer the given amount of times, or false otherwise.
     */
    final protected ArrayList<Boolean> advertise(ArrayList<Integer> buyersToConvince, int times) {
        ArrayList<Boolean> isChargedForBuyer = new ArrayList<Boolean>(buyersToConvince.size());
        for (int buyerId : buyersToConvince) {
            if (times <= 0) {
                isChargedForBuyer.add(false);
                continue;
            }
            if (charge(campaign.config.getAdvertisingCharge() * times)) {
                campaign.getBuyer(buyerId).convince(id, times);
                isChargedForBuyer.add(true);
            } else {
                isChargedForBuyer.add(false);
            }
        }
        return isChargedForBuyer;
    }

    /**
     * Charges the company with some amount and deducts it from its budget
     *
     * @param amount to be deducted
     * @return a boolean that represents whether the operation succeeded or not
     */
    private boolean charge(double amount) {
        if (budget >= amount) {
            budget -= amount;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Should implement the company's strategy using the research and advertise methods
     * It is called every day of the campaign.
     */
    public abstract void strategize();

    /**
     * Override this method in your subclass to add initialization code
     */
    protected void initialize() {
    }
}
