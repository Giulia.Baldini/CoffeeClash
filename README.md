Please refer to the [Problem Statement](Problem_Statement.pdf) for a more precise description of the task.

# CoffeeClash 
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

It’s winter, and in Germany it is currently very cold. What everybody really desires is a warm, delicious cup of coffee. The question is: Which coffee?

## To Starbucks and beyond!
You are the head of the marketing team of a very famous coffee company. 
You are not quite Starbucks yet, but you are aiming high. 
You have a fixed amount of days to advertise and do market research to ensure that many people buy your coffee. 
Your aim is to beat the other coffee companies.


## Want to play?
First, make sure the code works on your machine by running:
```bash
make all
```
All good? Let's start!

You can implement your strategy by creating a company class `Company_yourcompanyname` that 
extends the class `Company` and implementing the `strategize` method. But that's too much, right?
You can just run 
```bash
make company name=YOURSTRATEGYNAME
```
which will create a class called `Company_YOURSTRATEGYNAME` that already contains everything you need to start implementing your strategy.

### Company's built in methods
#### Get your budget
```java
this.getBudget() // Returns your current budget
```
#### What day is it?
```java
this.getCurrentDay() // Returns your current day
```
#### Do some marketing
```java
this.advertise(ArrayList<Integer> buyersToConvince, int times) // Convinces each buyer for a given number of times
```
#### Do some research
```java
this.research(ArrayList<Integer> buyersToAsk) // Returns a list of the company IDs that the buyers prefer
```

## Marketing Campaign
It is defined by a set of parameters (that are passed using the config class `MarketingConfig`):
* `budgetLowerLimit`, `budgetUpperLimit`: the range of the companies budgets, during initalization 
a number is drawn randomly from that range.
* `days`: number of days of the campaign.
* `buyersCount`: number of buyers.
* `advertisingCharge`: price a company pays to convince a buyer of their product.
* `researchCharge`: price a company pays to ask a buyer which company's product they prefer.

To retrieve these values, you can use
```java
this.config // Returns a MarketingConfig instance that has all the game configurations
```
plus a getter for the parameter you would like to retrieve. For example, for the `buyersCount` it would be
```java
this.config.getBuyersCount() // Returns the number of buyers participating in the campaign
```
## Campaign run
Here's an example of something you can do in the main in the [campaign](MarketingCampaign.java) class.
```java
int numTrials = 100; // Number of trials to run

MarketingConfig config = new MarketingConfig(
	15000,  // budgetLowerLimit
	45000,  // budgetUpperLimit
	500,    // buyersCount
	200,    // days
	3,      // advertisingCharge
	1       // researchCharge
);

// Initialize set of strategies to run on
ArrayList<String> strategies = new ArrayList<String>(Arrays.asList(
	"Company_convincing",
	"Company_selectedBuyers"
));

// Generate map that specifies 1 of each strategy 
Map<String, Integer> strategyCount = Utility.generateNOfEach(strategies, 1);

// Alternatively to initialization and generateNOfEach
// Read the pairs "strategy, numTimes" from file (example in strategies.txt)
// If there is no comma or the second term is empty, we assume it is 1
ArrayList<String> strategyCount = Utility.readFromFile("strategies.txt");

// Initiate campaign instance 
MarketingCampaign campaign = new MarketingCampaign(config, strategyCount);

// Get results
double[] res = campaign.run(this.numTrials);

// Print results
campaign.printScores(res);
```

### Printing
You can choose to print: 
- Score of each instance by using `printScores()`
- Average scores of each strategy by using `printAverages()`

But if that doesn't suit your printing needs you can create your own printing function, 
A helpful method for that is `companyStrategyNames` (in the `MarketingCampaign` class) 
that returns the ordered list of companies. Also, if your printing method outputs one number
for each strategy you can use our pretty-print method `Utility.prettyPrintResults()` 


### Sample Strategies
We have provided two sample strategies: [convincing](Company_convincing.java) and [selectedBuyers](Company_selectedBuyers.java). convincing advertises once to each buyer every day. 
The strategy of selectedBuyers is a bit more sophisticated, in fact they research all the buyers, and only advertise to those who are not currently returning their ID.

## Benchmarking
You can find one ready-made benchmark for you to test the performance of your strategies. 
If you feel like creating your own (and we suggest you do) just run 
```bash
make benchmark name=YOURBENCHMARKNAME
```
and follow the instructions in the generated `benchmark_YOURBENCHMARKNAME.java` file.

Your main job is to implement the `runBenchmark` method in the generated file
```java
public void runBenchmark(ArrayList<String> strategies, Object... params) {
    // Implement your own benchmark
}
```

The set of strategies is passed as a list (not a set) for consistency. So,
we're leaving the responsibility of not passing duplicates to you.

All generated benchmarks extend from the benchmark class and we have created two methods in the `Utility` class that 
may help you in creating your own benchmarks.

The first one is `generateNOfEach(ArrayList<String> strategies, int n)`, which can be used to generate `n` players for each strategy
in the `strategies` ArrayList. 
Alternatively, you can use `generateNOfEach(ArrayList<String> strategies, int[] nVector)`
if you want to have a different number of players for each strategy. Both methods return a `Map<String, Integer>`, which is
the input for the `run(int numTrials)` method of `MarketingCampaign`.

### Naming a benchmark
You can pass a name to your benchmark instance when calling it to specify 
different instances. If not passed it will use the `DEFAULT_NAME` defined in 
the benchmark class
```java
new Benchmark_X("First benchmark X", numTrials, config).run(setOfStratgies);
```

### Accessing params
You can access (optional) parameters in your benchmark by accessing the 
params array and intializing your parameter (of any type) in the `runBenchmark` method. 
```java
TYPENAME first_parameter = (TYPENAME) params[0];
```
Example: 
```java
int first_parameter = (int) params[0];
```

### Running a benchmark
Assuming you have written a mysterious benchmark X in the main method, you would call
it like this
```java
// With no parameters
new Benchmark_X(numTrials, config).run(setOfStratgies); 
// With parameters
new Benchmark_X(numTrials, config).run(setOfStratgies, first_parameter, second_parameter);
``` 

Another option is to read the strategies and how many times they should be played from file.
To do so, you can use
```java
// If you want to read from file in your benchmark
new Benchmark_X(numTrials, config).run(null, first_parameter, second_parameter);
```

And then add 
```java
Map<String, Integer> strategyCount = Utility.readFromFile("strategies.txt");
``` 
in your benchmark class. Your [strategies.txt](strategies.txt) file should look like this
```java
convincing, 1
selectedBuyers
``` 
If you do not put a comma or leave the second argument empty, we will assume the number of players is 1.

That's all, happy coding! 

## Credits
 Made by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam as a 
 part of requirement for the Advanced Algorithms lab at
 the University of Bonn (WS19/20).
