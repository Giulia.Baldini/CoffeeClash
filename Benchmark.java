// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This file is the template for the benchmarks and it should not be modified
 */
public abstract class Benchmark {
    private String name;
    protected int numTrials;
    protected MarketingConfig config;
    private static Calendar cal = Calendar.getInstance();
    private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private long timeInit;

    /**
     * Constructor for the Benchmark class
     *
     * @param name      the name that appears in the prints of your benchmark
     * @param numTrials how many times this benchmark should be run
     * @param config    the config you want to run this benchmark on
     */
    public Benchmark(String name, int numTrials, MarketingConfig config) {
        this.name = name;
        this.numTrials = numTrials;
        this.config = config;
    }

    /**
     * Prints some information about the start time of the benchmark
     */
    private void printInit() {
        timeInit = System.currentTimeMillis();
        Utility.printInColor("Running benchmark: " + name + ", starting at " + sdf.format(cal.getTime()) + ".\n");
    }

    /**
     * Prints some information about the finish time of the benchmark
     */
    private void printFinal() {
        long timeEnd = System.currentTimeMillis();
        Utility.printInColor("Benchmark " + name + " finished running at " + sdf.format(cal.getTime()) + "." +
            " It took " + (timeEnd - timeInit) + " ms.");
    }

    /**
     * Wrapper for runBenchmark method that prints relevant info before and after running the benchmark code
     */
    protected void run(ArrayList<String> strategies, Object... params) {
        printInit();
        runBenchmark(strategies, params);
        printFinal();
    }

    /**
     * Does the required computation of the benchmark, to be implemented in child class
     *
     * @param strategies set of strategies to run the benchmark on
     * @param params     (optional) pass other params to your benchmark, can be used inside method as an array
     */
    protected abstract void runBenchmark(ArrayList<String> strategies, Object... params);
}
