// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;
import java.util.Map;

/**
 * This is an example benchmark
 */
public class Benchmark_oneEach extends Benchmark {
    private static final String DEFAULT_NAME = "One Each";

    /**
     * Constructor for the Benchmark_oneEach class
     *
     * @param name      the name that appears in the prints of your benchmark
     * @param numTrials how many times this benchmark should be run
     * @param config    the config you want to run this benchmark on
     */
    public Benchmark_oneEach(String name, int numTrials, MarketingConfig config) {
        super(name, numTrials, config);
    }

    /**
     * Overloaded constructor, in case you want all the benchmarks of this type to be called the same
     * In case you use this constructor, we use DEFAULT_NAME for the prints.
     *
     * @param numTrials how many times this benchmark should be run
     * @param config    the config you want to run this benchmark on
     */
    public Benchmark_oneEach(int numTrials, MarketingConfig config) {
        super(DEFAULT_NAME, numTrials, config);
    }

    /**
     * Does the required computation of the benchmark
     *
     * @param strategies set of strategies to run the benchmark on
     * @param params     (optional) pass other params to your benchmark, in this case is not needed.
     */
    public void runBenchmark(ArrayList<String> strategies, Object... params) {
        Map<String, Integer> strategyCount = Utility.generateNOfEach(strategies, 1);

        // Use the following method if you want to read from file
        // Map<String, Integer> strategyCount = Utility.readFromFile("strategies.txt");

        MarketingCampaign campaign = new MarketingCampaign(this.config, strategyCount);
        double[] res = campaign.run(this.numTrials);
        campaign.printScores(res);
    }
}
