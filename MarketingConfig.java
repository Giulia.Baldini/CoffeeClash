// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

public class MarketingConfig {
    private double budgetLowerLimit;
    private double budgetUpperLimit;
    private int buyersCount;
    private int days;
    private double advertisingCharge;
    private double researchCharge;

    /**
     * Creates a marketing configuration
     *
     * @param budgetLowerLimit  the lower bound of where the budget can be drawn from
     * @param budgetUpperLimit  the upper bound of where the budget can be drawn from
     * @param buyersCount       the number of buyers participating in the campaign
     * @param days              the number of days of the campaign
     * @param advertisingCharge the fee to pay to advertise to one buyer
     * @param researchCharge      the fee to pay to do market research on one buyer
     */
    public MarketingConfig(double budgetLowerLimit, double budgetUpperLimit, int buyersCount, int days, double advertisingCharge, double researchCharge) {
        this.budgetLowerLimit = budgetLowerLimit;
        this.budgetUpperLimit = budgetUpperLimit;
        this.buyersCount = buyersCount;
        this.days = days;
        this.advertisingCharge = advertisingCharge;
        this.researchCharge = researchCharge;
    }

    /**
     * @return the lower bound of where the budget can be drawn from
     */
    public double getBudgetLowerLimit() {
        return budgetLowerLimit;
    }

    /**
     * @return the upper bound of where the budget can be drawn from
     */
    public double getBudgetUpperLimit() {
        return budgetUpperLimit;
    }

    /**
     * @return the number of buyers participating in the campaign
     */
    public int getBuyersCount() {
        return buyersCount;
    }

    /**
     * @return the number of days of the campaign
     */
    public int getDays() {
        return days;
    }

    /**
     * @return the fee to pay to advertise to one buyer
     */
    public double getAdvertisingCharge() {
        return advertisingCharge;
    }

    /**
     * @return the fee to pay to do market research on one buyer
     */
    public double getResearchCharge() {
        return researchCharge;
    }

}
