// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * This class represents a buyer in our game and it should not be accessed by the classes that extend Company
 */
public class Buyer {
    private int id;
    private double[] updatedCompanyWeights;
    private double[] companyWeights;

    /**
     * Constructor for the Buyer class
     *
     * @param id           ID of the buyer
     * @param companyCount number of companies (to initialize the companyWeights)
     */
    public Buyer(int id, int companyCount) {
        this.id = id;
        companyWeights = new double[companyCount];
        updatedCompanyWeights = new double[companyCount];
        // initialize all weights to 1
        for (int i = 0; i < companyCount; i++) {
            companyWeights[i] = 1;
            updatedCompanyWeights[i] = 1;
        }
    }

    /**
     * Getter for the ID of a buyer
     *
     * @return the ID of the buyer
     */
    public int getId() {
        return id;
    }

    /**
     * Getter for the company weights of a buyer
     *
     * @return an array of doubles containing the weights
     */
    public double[] getCompanyWeights() {
        return companyWeights;
    }

    /**
     * Updates the weights of a buyer with respect to certain company
     *
     * @param companyId the company for which the weight should be increased
     * @param amount    by how much should the weight be increased
     */
    public void convince(int companyId, double amount) {
        updatedCompanyWeights[companyId] += amount;
    }

    /**
     * Gets most preferred company
     *
     * @return companyId for the company that has the maximum weight
     */
    public int ask() {
        double maxWeight = 0;
        ArrayList<Integer> companiesWithMaxWeight = new ArrayList<>();
        for (int companyId = 0; companyId < companyWeights.length; ++companyId) {
            if (companyWeights[companyId] > maxWeight) {
                maxWeight = companyWeights[companyId];
                companiesWithMaxWeight = new ArrayList<>(Arrays.asList(companyId));
            } else if (companyWeights[companyId] == maxWeight) {
                companiesWithMaxWeight.add(companyId);
            }
        }
        return getRandomElement(companiesWithMaxWeight);
    }

    /**
     * Given a list of company IDs, returns a random ID
     *
     * @param list list of companies with the same weight
     * @return a random company
     */
    private int getRandomElement(ArrayList<Integer> list) {
        Random generator = new Random();
        return list.get(generator.nextInt(list.size()));
    }

    /**
     * Updates the weights after the companies have played their strategy on that day
     */
    public void updateWeights() {
        companyWeights = updatedCompanyWeights;
    }


}
