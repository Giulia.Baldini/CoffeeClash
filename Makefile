# Lab Algorithimic Game Theory, University of Bonn
# Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

all: compile run
	@echo "All good!"

compile: MarketingCampaign.class

run:
	@echo "Running... \nbeep bop..."
	@java -ea -cp ./out MarketingCampaign
	@echo "Run complete.\n--------------"

MarketingCampaign.class: MarketingCampaign.java *.java
	@echo "Compiling... \nbeep bop..."
	@javac -d ./out -Xlint MarketingCampaign.java *.java
	@echo "Compilation complete.\n--------------"

clean:
	@echo "Removing output files..."
	@rm -rf ./out/*.class
	@echo "Done... all clean!\n--------------"

# example call: make company name=X
company:
ifndef name
	$(error You don't want your company to be nameless, do you?)
else
	@sed 's/STRATEGYNAME/$(name)/' COMPANY_TEMPLATE.tmpl > Company_$(name).java
	@echo Company_$(name).java generated
endif

# example call: make benchmark name=Y
benchmark:
ifndef name
	$(error You don't want your benchmark to be nameless, do you?)
else
	@sed 's/BENCHMARKNAME/$(name)/' BENCHMARK_TEMPLATE.tmpl > Benchmark_$(name).java
	@echo Benchmark_$(name).java generated
endif