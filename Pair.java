// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.AbstractMap;
import java.util.Map;

class Pair {
    /**
     * A 2-tuple like structure based on Map.Entry
     * @param first first item in pair
     * @param second second item in pair
     * @param <T> type of first item
     * @param <U> type of second item
     * @return a Map.entry pair of items
     */
    public static <T, U> Map.Entry<T, U> of(T first, U second) {
        return new AbstractMap.SimpleEntry<>(first, second);
    }
}