// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class MarketingCampaign {
    public MarketingConfig config;
    private int currentDay;
    private ArrayList<Buyer> buyers;
    private ArrayList<Company> companies;
    private Map<String, Integer> strategies;

    /**
     * Constructor for the MarketingCampaign class
     *
     * @param config     configuration to run
     * @param strategies a Map containing the strategies you would like to run, and how many players per strategy
     */
    public MarketingCampaign(MarketingConfig config, Map<String, Integer> strategies) {
        this.config = config;
        this.strategies = strategies;
        initialize();
    }

    /**
     * Initializes the companies and the buyers
     */
    private void initialize() {
        createCompanies(strategies);
        createBuyers();
    }

    /**
     * Given a map, it creates an ArrayList of companies
     *
     * @param strategies a Map containing the strategies you would like to run, and how many players per strategy
     */
    private void createCompanies(Map<String, Integer> strategies) {
        companies = new ArrayList<>();
        int id = 0;
        for (String strategyName : strategies.keySet()) {
            for (int i = 0; i < strategies.get(strategyName); i++) {
                double budget = Utility.drawBudget(config.getBudgetLowerLimit(), config.getBudgetUpperLimit());
                companies.add(Utility.strategyFromName(strategyName, id, budget, this));
                ++id;
            }
        }
    }

    /**
     * Creates an ArrayList of buyers
     */
    private void createBuyers() {
        buyers = new ArrayList<Buyer>();
        for (int i = 0; i < config.getBuyersCount(); i++) {
            buyers.add(new Buyer(i, companies.size()));
        }
    }

    /**
     * When the day of campaign is over, it updates the weights and updates the score counters for the companies
     *
     * @param res an array containing the current scores of each company
     */
    private void endDay(double[] res) {
        for (Buyer buyer : buyers) {
            buyer.updateWeights();
            int currWinner = buyer.ask();
            ++res[currWinner];
        }
    }

    /**
     * Represents one run of our campain over a certain number of days.
     * Each day, it changes the day, calls strategize() for each company and calls endDay() to update the values
     *
     * @return an array containing the normalized scores for each company
     */
    private double[] runOnce() {
        initialize();

        double[] scores = new double[companies.size()];

        for (int day = 1; day <= config.getDays(); ++day) {
            currentDay = day;
            for (Company company : companies) {
                company.strategize();
            }

            endDay(scores);
        }

        return normalizeScoresByDaysAndBuyersCount(scores);
    }

    /**
     * Given an array of scores, it normalizes them by the number of days and number of buyers
     *
     * @param scores the current payoff for each company
     * @return the normalized payoff
     */
    private double[] normalizeScoresByDaysAndBuyersCount(double[] scores) {
        double[] normalizedScores = new double[scores.length];
        for (int i = 0; i < normalizedScores.length; i++) {
            normalizedScores[i] = scores[i] / (config.getDays() * config.getBuyersCount());
        }
        return normalizedScores;
    }

    /**
     * Repeats a run numTrials time
     *
     * @param numTrials the number of times the simulation should be repeated
     * @return the average payoff over all runs
     */
    public double[] run(int numTrials) {
        double[] res = new double[companies.size()];

        for (int i = 0; i < numTrials; ++i) {
            double[] tempRes = runOnce();
            for (int j = 0; j < res.length; ++j) {
                res[j] += tempRes[j];
            }
        }

        for (int i = 0; i < res.length; ++i) {
            res[i] /= numTrials;
        }

        return res;
    }

    /**
     * Getter for the current day of the campaign
     *
     * @return the current day of the campaign
     */
    public int getCurrentDay() {
        return currentDay;
    }

    /**
     * Given a buyer ID, returns the Buyer object
     *
     * @param buyerId
     * @return Buyer object with that ID
     */
    public Buyer getBuyer(int buyerId) {
        return buyers.get(buyerId);
    }

    // Three print methods

    /**
     * Prints all companies and their scores
     *
     * @param res the payoff array
     */
    public void printScores(double[] res) {
        Utility.printScores(res, companies);
    }

    /**
     * Prints the average score in case a company has multiple instances
     *
     * @param res        the payoff array
     */
    public void printAverages(double[] res) {
        Utility.printAverages(res, strategies, companies);
    }

    /**
     * Returns an ArrayList contaning the company names of the company ArrayList
     *
     * @return an ArrayList containg the strategy names
     */
    public ArrayList<String> companyStrategyNames() {
        return Utility.companyStrategyNames(companies);
    }

    /**
     * Runs the program
     *
     * @param args None
     */
    public static void main(String[] args) {
        int numTrials = 100;

        int factor = 1;
        MarketingConfig config = new MarketingConfig(
            150000,
            450000,
            500,
            200,
            factor * 3.0,
            factor * 1.0
        );

        ArrayList<String> strategies = new ArrayList<String>(Arrays.asList(
            "Company_convincing",
            "Company_selectedBuyers"
        ));

        // You can set strategies to null if you want to read from file in the benchmark
        // new Benchmark_oneEach(numTrials, config).run(null);

        new Benchmark_oneEach(numTrials, config).run(strategies);

    }
}
