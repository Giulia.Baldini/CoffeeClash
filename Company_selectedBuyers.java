// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;

public class Company_selectedBuyers extends Company {
    ArrayList<Integer> buyers;

    /**
     * Constructor for the Company_convincing class
     *
     * @param id       the ID of the company
     * @param budget   the budget that a company can spend during the time period
     * @param campaign the campaign object where this company will run
     */
    public Company_selectedBuyers(int id, double budget, MarketingCampaign campaign) {
        super(id, budget, campaign);
    }

    /**
     * Some initialization code: create an ArrayList containing the buyer IDs
     */
    protected void initialize() {
        int numBuyers = this.config.getBuyersCount();
        buyers = new ArrayList<>();
        for (int i = 0; i < numBuyers; i++) {
            buyers.add(i);
        }
    }

    /**
     * Example strategy: advertise to the buyers that would not buy my product
     */
    public void strategize() {
        ArrayList<Integer> ids = research(buyers);

        ArrayList<Integer> buyersNotBuyingMe = new ArrayList<>(buyers);

        for (int i = ids.size() - 1; i > -1; --i) {
            if (ids.get(i) == getId()) {
                buyersNotBuyingMe.remove(i);
            }
        }

        advertise(buyersNotBuyingMe, 1);
    }
}
