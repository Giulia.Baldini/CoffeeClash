// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains methods that are useful for MarketingCampaign and for strategy design
 */

public class Utility {


    /***
     *      ____       _       _   _               __  __      _   _               _
     *     |  _ \ _ __(_)_ __ | |_(_)_ __   __ _  |  \/  | ___| |_| |__   ___   __| |___
     *     | |_) | '__| | '_ \| __| | '_ \ / _` | | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
     *     |  __/| |  | | | | | |_| | | | | (_| | | |  | |  __/ |_| | | | (_) | (_| \__ \
     *     |_|   |_|  |_|_| |_|\__|_|_| |_|\__, | |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
     *                                     |___/
     *      This section of the class contains all the printing methods
     */


    // Color codes
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_GREEN = "\u001B[32m";

    /**
     * Prints a string in color
     *
     * @param string the string to be printed
     */
    public static void printInColor(String string) {
        String ansi_color = ANSI_GREEN;
        System.out.println(ansi_color + string + ANSI_RESET);
    }

    /**
     * Prints the scores of the companies by computing the average over the ones of the same strategy
     *
     * @param res        the array of current results (scores) of companies
     * @param strategies the Map containing how many players each strategy has
     */
    public static void printAverages(double[] res, Map<String, Integer> strategies, ArrayList<Company> companies) {
        Map<String, Double> stratResults = new HashMap<>();

        for (String strategy : strategies.keySet()) {
            if (strategies.get(strategy) > 0) {
                stratResults.put(strategy, 0.0);
            }
        }

        for (int i = 0; i < res.length; i++) {
            String stratName = companies.get(i).getClass().getName();
            stratResults.put(stratName, stratResults.get(stratName) + res[i]);
        }

        for (String strategy : stratResults.keySet()) {
            stratResults.put(strategy, stratResults.get(strategy) / strategies.get(strategy));
        }

        // Sort average scores
        List<Map.Entry<String, Double>> averages = new ArrayList<>(stratResults.entrySet());
        averages.sort((p1, p2) -> p2.getValue().compareTo(p1.getValue()));

        // Print average scores
        System.out.println("Average Scores:\n--------------");
        prettyPrintResults(averages);
    }

    /**
     * Prints the scores of the companies one by one
     *
     * @param res the array of current scores (payoffs) of the companies
     */
    public static void printScores(double[] res, ArrayList<Company> companies) {
        ArrayList<Map.Entry<String, Double>> companyResults = new ArrayList<>();

        // group strategy with its result
        for (int i = 0; i < res.length; i++) {
            companyResults.add(Pair.of(companies.get(i).getClass().getName().substring(8), res[i]));
        }

        // sort scores in descending order
        companyResults.sort((p1, p2) -> p2.getValue().compareTo(p1.getValue()));

        // print sorted scores
        System.out.println("Strategy Scores:\n--------------");
        prettyPrintResults(companyResults);
    }

    /**
     * Prints results in a nice format
     * @param strategyResults a Map from strategy names to their results
     */
    private static void prettyPrintResults(List<Map.Entry<String, Double>> strategyResults) {
        for (Map.Entry<String, Double> strategyResult : strategyResults) {
            System.out.println(" - " +
                strategyResult.getKey() +
                ", " +
                strategyResult.getValue()
            );
        }
        System.out.println();
    }


    /***
     *       ____                           _   __  __      _   _               _
     *      / ___| ___ _ __   ___ _ __ __ _| | |  \/  | ___| |_| |__   ___   __| |___
     *     | |  _ / _ \ '_ \ / _ \ '__/ _` | | | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
     *     | |_| |  __/ | | |  __/ | | (_| | | | |  | |  __/ |_| | | | (_) | (_| \__ \
     *      \____|\___|_| |_|\___|_|  \__,_|_| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
     *
     *       This section contains all methods that are useful in general
     */


    /**
     * Generates a list of company strategy names
     *
     * @param companies companies array
     * @return an ArrayList containing company strategy names
     */
    public static ArrayList<String> companyStrategyNames(ArrayList<Company> companies) {
        ArrayList<String> names = new ArrayList<>();
        for (Company company : companies) {
            names.add(company.getClass().getName());
        }
        return names;
    }


    /**
     * Generates a strategy count HashMap for the given strategies
     *
     * @param strategies set of strategies to generate from
     * @param n          number of times to add each strategy
     */
    public static Map<String, Integer> generateNOfEach(ArrayList<String> strategies, int n) {
        HashMap<String, Integer> players = new HashMap<>();

        for (String strategy : strategies) {
            players.put(strategy, n);
        }

        return players;
    }

    /**
     * Generates a strategy count HashMap for the given strategies
     *
     * @param strategies set of strategies to generate from
     * @param nVector    vector containing the number of times to add each strategy for each position
     */
    public static Map<String, Integer> generateNOfEach(ArrayList<String> strategies, int[] nVector) {
        HashMap<String, Integer> players = new HashMap<>();

        for (int i = 0; i < strategies.size(); i++) {
            players.put(strategies.get(i), nVector[i]);
        }

        return players;
    }


    /***
     *      _____ _ _        __  __      _   _               _
     *     |  ___(_) | ___  |  \/  | ___| |_| |__   ___   __| |___
     *     | |_  | | |/ _ \ | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
     *     |  _| | | |  __/ | |  | |  __/ |_| | | | (_) | (_| \__ \
     *     |_|   |_|_|\___| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
     *
     *      This section contains methods used for file reading
     */

    /**
     * Given a string, removes all space characters
     *
     * @param chars a string
     * @return the string without space characters
     */
    private static String removeSpaces(String chars) {
        return chars.replaceAll("\\s+", "");
    }

    /**
     * Reads a file and returns a Map containing the strategies and how many players per strategy
     *
     * @param filename the file to read the strategies from
     * @return a Map containing the strategies and how many players per strategy
     */
    public static Map<String, Integer> readFromFile(String filename) {
        BufferedReader namesFile = null;
        HashMap<String, Integer> strategyCounts = new HashMap<>();

        // Use the following filepath string if your project does not have the CoffeClash folder as main folder
        // String filepath = System.getProperty("user.dir") + "/CoffeeClash/" + filename;
        try {
            namesFile = new BufferedReader(new FileReader(filename));
            String line = null;
            while ((line = namesFile.readLine()) != null) {
                String[] info = line.split(",");
                String strategyName = "Company_" + removeSpaces(info[0]);
                int numPlayers = 1;
                if (info.length == 2) {
                    numPlayers = Integer.parseInt(removeSpaces(info[1]));
                }
                strategyCounts.put(strategyName, numPlayers);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return strategyCounts;
    }


    /***
     *       ____                            _               __  __      _   _               _
     *      / ___|__ _ _ __ ___  _ __   __ _(_) __ _ _ __   |  \/  | ___| |_| |__   ___   __| |___
     *     | |   / _` | '_ ` _ \| '_ \ / _` | |/ _` | '_ \  | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
     *     | |__| (_| | | | | | | |_) | (_| | | (_| | | | | | |  | |  __/ |_| | | | (_) | (_| \__ \
     *      \____\__,_|_| |_| |_| .__/ \__,_|_|\__, |_| |_| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
     *                          |_|            |___/
     *
     *      This section is concerned with methods used only inside the MarketCampaign class
     */

    /**
     * Creates a company instance for a given class name
     *
     * @param strategyName the name of the class
     * @param id           the ID of the company
     * @param budget       the initial budget of the company
     * @param campaign     the campaign object where this company will run
     * @return a Company object with the given information
     */
    public static Company strategyFromName(String strategyName, int id, double budget, MarketingCampaign campaign) {
        try {
            return (Company) Class.forName(strategyName)
                .getConstructor(int.class, double.class, MarketingCampaign.class)
                .newInstance(id, budget, campaign);
        } catch (ReflectiveOperationException roe) {
            throw new RuntimeException(roe);
        }
    }

    /**
     * Draws a budget uniformly at random between two values
     *
     * @param low  the budget lower bound
     * @param high the budget upper bound
     * @return a random value between the two given values
     */
    public static double drawBudget(double low, double high) {
        return Math.random() * (high - low + 1) + low;
    }
}
