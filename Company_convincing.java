// Algorithmic Game Theory Lab at the University of Bonn (WS1920)
// Created by Johanna Albrecht, Giulia Baldini, Mohammed Ghannam

import java.util.ArrayList;

public class Company_convincing extends Company {
    ArrayList<Integer> buyers;

    /**
     * Constructor for the Company_convincing class
     *
     * @param id       the ID of the company
     * @param budget   the budget that a company can spend during the time period
     * @param campaign the campaign object where this company will run
     */
    public Company_convincing(int id, double budget, MarketingCampaign campaign) {
        super(id, budget, campaign);
    }

    /**
     * Some initialization code: create an ArrayList containing the buyer IDs
     */
    protected void initialize() {
        int numBuyers = this.config.getBuyersCount();
        buyers = new ArrayList<>();
        for (int i = 0; i < numBuyers; i++) {
            buyers.add(i);
        }
    }

    /**
     * Example strategy: just advertise to each buyer once every day
     */
    public void strategize() {
        advertise(buyers, 1);
    }
}
